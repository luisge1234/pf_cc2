/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestingCalculoCocomo;

import controlador.Calculo_COCOMOIIControlador;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import junit.framework.Assert;
//import modelo.Empleado;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author lgmoc
 */
public class CocomoIITest {

    private Calculo_COCOMOIIControlador calculo_COCOMOIIControlador;

    @Before
    public void setUp() {
        calculo_COCOMOIIControlador = new Calculo_COCOMOIIControlador();
    }

    //tiempo de desarrollo
    @Test
    public void calcular_TiempoDesarrollo_FlujoNormal() {
        double numEsf = 100.00;
        double numFact = 100.00;
        double resp = calculo_COCOMOIIControlador.calcular_TiempoDesarrollo(numEsf, numFact);
        //compara lo q envio con lo q recibo
        NumberFormat formatter = new DecimalFormat("#0.00");

        Assert.assertEquals("33,47", formatter.format(resp));
    }

    @Test(expected = NumberFormatException.class)
    public void calcular_TiempoDesarrollo_ValorNegativo() {
        double numEsf = 100.00;
        double numFact = -100.00;
        calculo_COCOMOIIControlador.calcular_TiempoDesarrollo(numEsf, numFact);
    }

    //tiempo Esfuerzo calcular_Esfuerzo
    @Test
    public void calcular_Esfuerzo_FlujoNormal() {
        double Ksloc = 0.25;
        double B = 1.18;
        double Fac_Personal = 1.35;
        double Fac_Proyecto = 0.95;
        double Fac_Plataforma = 1.26;
        double Fac_Producto = 1.23;
        double resp = calculo_COCOMOIIControlador.calcular_Esfuerzo(Ksloc, B, Fac_Personal, Fac_Plataforma, Fac_Producto, Fac_Proyecto);
        //compara lo q envio con lo q recibo
        NumberFormat formatter = new DecimalFormat("#0.00");

        Assert.assertEquals("1,14", formatter.format(resp));
    }

    // calcular_costo del modulo
    @Test
    public void calcular_CostoModulo_FlujoNormal() {
        double Sueldo = 480;
        double TiempoDesarrollo = 3.18;
        double CantPersonas = 1;

        double resp = calculo_COCOMOIIControlador.calcular_CostoModulo(Sueldo, TiempoDesarrollo, CantPersonas);
        //compara lo q envio con lo q recibo
        NumberFormat formatter = new DecimalFormat("#0.00");

        Assert.assertEquals("2098,80", formatter.format(resp));
    }

    @Test(expected = NumberFormatException.class)
    public void calcular_CostoModulo_SueldoNegativo() {
        double Sueldo = -480;
        double TiempoDesarrollo = 3.18;
        double CantPersonas = 1;
        double resp = calculo_COCOMOIIControlador.calcular_CostoModulo(Sueldo, TiempoDesarrollo, CantPersonas);;
    }

    // calcular_total de imprevistos
    @Test
    public void calcular_Imprevistos_FlujoNormal() {
        double Sueldo = 480;
        double TiempoDesarrollo = 3.18;
        double CantPersonas = 1;

        double resp = calculo_COCOMOIIControlador.calcular_Imprevistos(Sueldo, TiempoDesarrollo, CantPersonas);
        //compara lo q envio con lo q recibo
        NumberFormat formatter = new DecimalFormat("#0.00");

        Assert.assertEquals("190,80", formatter.format(resp));
    }

    @Test(expected = NumberFormatException.class)
    public void calcular_Imprevistos_SueldoNegativo() {
        double Sueldo = -480;
        double TiempoDesarrollo = 3.18;
        double CantPersonas = 1;
        double resp = calculo_COCOMOIIControlador.calcular_Imprevistos(Sueldo, TiempoDesarrollo, CantPersonas);;
    }

    // Prueba del metodo calcularFactorDeMultiplicacion
    @Test
    public void calcularFactorEscala5FlujoNormal() {
        double Prec = 6.20;
        double Flex = 4.05;
        double Resl = 4.24;
        double Team = 2.19;
        double Pmat = 1.56;
        double resp = calculo_COCOMOIIControlador.calcularFactorEscala5(Prec, Flex, Resl, Team, Pmat);
        NumberFormat formatter = new DecimalFormat("#0.00");
        Assert.assertEquals("18,24", formatter.format(resp));
    }

    //Pruebas del metodo para obtener el factor de multiplicacion
    @Test
    public void calcularFactorDeMultiplicacionFlujoNormal() {
        double suma = 18.24;
        double resp = calculo_COCOMOIIControlador.calcularFactorDeMultiplicacion(suma);
        NumberFormat formatter = new DecimalFormat("#0.00");
        Assert.assertEquals("1,09", formatter.format(resp));
    }

    @Test(expected = NumberFormatException.class)
    public void calcularFactorDeMultiplicacionSumaNegativa() {
        double suma = -18.24;
        double resp = calculo_COCOMOIIControlador.calcularFactorDeMultiplicacion(suma);
    }

    //Pruebas del metodo para obtener los puntos de funcion ajustados
    @Test
    public void calcularPFAFlujoNormal() {
        double pfna = 18.00;
        double fc = 35.00;
        double resp = calculo_COCOMOIIControlador.calcularPFA(pfna, fc);
        NumberFormat formatter = new DecimalFormat("#0.00");
        Assert.assertEquals("18,00", formatter.format(resp));
    }

    @Test(expected = NumberFormatException.class)
    public void calcularPFAValoresNegativos() {
        double pfna = -18.00;
        double fc = 35.00;
        double resp = calculo_COCOMOIIControlador.calcularPFA(pfna, fc);
    }

    //Pruebas del metodo para obtener la cantidad de personas 
    @Test
    public void calcularCantidadPersonasFlujoNormal() {
        double esfuerzo = 51.14;
        double tiempodesarrollo = 33.47;
        double resp = calculo_COCOMOIIControlador.calcularCantidadPersonas(esfuerzo, tiempodesarrollo);
        DecimalFormat formatter = new DecimalFormat("#0");
        Assert.assertEquals("2", formatter.format(resp));
    }

    @Test(expected = NumberFormatException.class)
    public void calcularCantidadPersonasValoresNegativos() {
        double esfuerzo = -51.14;
        double tiempodesarrollo = 33.47;
        double resp = calculo_COCOMOIIControlador.calcularCantidadPersonas(esfuerzo, tiempodesarrollo);
    }
}
