/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestingCalculoCocomo;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import controlador.Modelo_PostArquitecturaControlador;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author krlat
 */
public class TestingModeloPostArquitectura {

    private Modelo_PostArquitecturaControlador modelo_PostArquitecturaControlador;

    @Before
    public void setUP() {
        modelo_PostArquitecturaControlador = new Modelo_PostArquitecturaControlador();
    }

    //Test Factores de costo Factor Producto 
    @Test
    public void calcular_FactoresProducto_FlujoNormal() {
        double numFactProdRELY = 1.10;
        double numFactProdDATA = 1.14;
        double numFactProdDOCU = 1.11;
        double numFactProdCPLX = 1.17;
        double numFactProdRUSE = 1.07;

        double total = modelo_PostArquitecturaControlador.calcular_FactoresProducto(numFactProdRELY, numFactProdDATA, numFactProdDOCU, numFactProdCPLX, numFactProdRUSE);
        NumberFormat formatter = new DecimalFormat("#0.00");
        Assert.assertEquals("1,74", formatter.format(total));
    }

    // //Test Factores de costo Factor Plataforma
    @Test
    public void calcular_FactoresPlataforma_FlujoNormal() {
        double numFacEstrTIME = 1.11;
        double numFacEstrSTOR = 1.05;
        double numFacEstrPVOL = 1.15;

        double total = modelo_PostArquitecturaControlador.calcular_FactoresPlataforma(numFacEstrTIME, numFacEstrSTOR, numFacEstrPVOL);
        NumberFormat formatter = new DecimalFormat("#0.00");
        Assert.assertEquals("1,34", formatter.format(total));
    }
    //Test Factores de costo Factor Personal

    @Test
    public void calcular_FactoresPersonal_FlujoNormal() {
        double numFactPersACAP = 0.71;
        double numFactPersAEXP = 0.81;
        double numFactPersPCAP = 0.76;
        double numFactPersPEXP = 0.85;
        double numFactPersLTEX = 0.84;
        double numFactPersPCON = 0.81;

        double total = modelo_PostArquitecturaControlador.calcular_FactoresPersonal(numFactPersACAP, numFactPersAEXP, numFactPersPCAP, numFactPersPEXP, numFactPersLTEX, numFactPersPCON);
        NumberFormat formatter = new DecimalFormat("#0.00");
        Assert.assertEquals("0,25", formatter.format(total));
    }
    //Test Factores de costo Factor Proyecto

    @Test
    public void calcular_FactoresProyecto_FlujoNormal() {
        double numFacProySTOOL = 0.90;
        double numFacProySCED = 1.00;
        double numFacProySITE = 0.93;

        double total = modelo_PostArquitecturaControlador.calcular_FactoresProyecto(numFacProySTOOL, numFacProySCED, numFacProySITE);
        NumberFormat formatter = new DecimalFormat("#0.00");
        Assert.assertEquals("0,84", formatter.format(total));
    }
}
