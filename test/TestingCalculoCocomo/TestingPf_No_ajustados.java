/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestingCalculoCocomo;

import controlador.PF_No_ajustadosControlador;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author krlat
 */
public class TestingPf_No_ajustados {

    private PF_No_ajustadosControlador pF_No_ajustadosControlador;

    @Before
    public void setUp() {
        pF_No_ajustadosControlador = new PF_No_ajustadosControlador();
    }

    //Test Puntos de Función no Ajustados Calcular EntradasExternas
    @Test
    public void calcularEntradasExternas_FlujoNormal() {
        int numEntradasExtBajo = 5;
        int numEntradasExtMedio = 3;
        int numEntradasExtAlto = 4;

        int total = pF_No_ajustadosControlador.calcularEntradasExternas(numEntradasExtBajo, numEntradasExtMedio, numEntradasExtAlto);
        Assert.assertEquals(51, total);
    }

    @Test(expected = NumberFormatException.class)
    public void calcularEntradasExternas_ValorNegativo() {
        int numEntradasExtBajo = -5;
        int numEntradasExtMedio = -3;
        int numEntradasExtAlto = -4;

        int total = pF_No_ajustadosControlador.calcularEntradasExternas(numEntradasExtBajo, numEntradasExtMedio, numEntradasExtAlto);

    }

    //Test Puntos de Función no Ajustados Calcular Entradas Internas
    @Test
    public void calcularEntradasInternas_FlujoNormal() {
        int numEntradasIntBajo = 2;
        int numEntradasIntMedio = 4;
        int numEntradasIntAlto = 6;

        int total = pF_No_ajustadosControlador.calcularSalidasExternas(numEntradasIntBajo, numEntradasIntMedio, numEntradasIntAlto);
        Assert.assertEquals(70, total);
    }

    @Test(expected = NumberFormatException.class)
    public void calcularEntradasInternas_ValorNegativo() {
        int numEntradasIntBajo = -2;
        int numEntradasIntMedio = -4;
        int numEntradasIntAlto = 6;

        int total = pF_No_ajustadosControlador.calcularSalidasExternas(numEntradasIntBajo, numEntradasIntMedio, numEntradasIntAlto);
    }

    //Test Puntos de Función no Ajustados Calcular Consulta Externas
    @Test
    public void calcularConsultasExternas_FlujoNormal() {
        int numConsultasExtBajo = 6;
        int numConsultasExtMedio = 3;
        int numConsultasExtAlto = 8;

        int total = pF_No_ajustadosControlador.calcularConsultasExternas(numConsultasExtBajo, numConsultasExtMedio, numConsultasExtAlto);
        Assert.assertEquals(78, total);
    }

    @Test(expected = NumberFormatException.class)
    public void calcularConsultasExternas_ValorNegativo() {
        int numConsultasExtBajo = -6;
        int numConsultasExtMedio = 3;
        int numConsultasExtAlto = 8;

        int total = pF_No_ajustadosControlador.calcularConsultasExternas(numConsultasExtBajo, numConsultasExtMedio, numConsultasExtAlto);

    }
    //Test Puntos de Función no Ajustados Calcular Archivos Logicos Internos

    @Test
    public void calcularArchivosLogicosInternos() {
        int numArchivosLogicosIntBajo = 5;
        int numArchivosLogicosIntMedio = 3;
        int numArchivosLogicosIntAlto = 1;

        int total = pF_No_ajustadosControlador.calcularArchivosLogicosInternos(numArchivosLogicosIntBajo, numArchivosLogicosIntMedio, numArchivosLogicosIntAlto);
        Assert.assertEquals(80, total);
    }

    @Test(expected = NumberFormatException.class)
    public void calcularArchivosLogicosInternos_ValorNegativo(){
        int numArchivosLogicosIntBajo = 5;
        int numArchivosLogicosIntMedio = 3;
        int numArchivosLogicosIntAlto = -1;

        int total = pF_No_ajustadosControlador.calcularArchivosLogicosInternos(numArchivosLogicosIntBajo, numArchivosLogicosIntMedio, numArchivosLogicosIntAlto);

    }
    
    //Test Puntos de Función no Ajustados Calcular Archivos Interfaz Externos 
    @Test
    public void calcularArchivosInterfazExternos() {
        int numArchivosInterfazExtBajo = 0;
        int numArchivosInterfazExtMedio = 4;
        int numArchivosInterfazExttAlto = 1;

        int total = pF_No_ajustadosControlador.calcularArchivosInterfazExternos(numArchivosInterfazExtBajo, numArchivosInterfazExtMedio, numArchivosInterfazExttAlto);
        Assert.assertEquals(38, total);
    }
    
    @Test(expected = NumberFormatException.class)
    public void calcularArchivosInterfazExternos_ValorNegativo(){
        int numArchivosInterfazExtBajo = 0;
        int numArchivosInterfazExtMedio = -4;
        int numArchivosInterfazExttAlto = 1;

        int total = pF_No_ajustadosControlador.calcularArchivosInterfazExternos(numArchivosInterfazExtBajo, numArchivosInterfazExtMedio, numArchivosInterfazExttAlto);
        
    }
}
