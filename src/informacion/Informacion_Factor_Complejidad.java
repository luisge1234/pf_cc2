/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package informacion;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 *
 * @author krlat
 */
public class Informacion_Factor_Complejidad {
    JFrame v = new JFrame("Factores Complejidad");
    public void Informacion_Factor_complejidad(){
     v.setPreferredSize(new Dimension(400, 750));
        JEditorPane editor = new JEditorPane();
        JFrame.setDefaultLookAndFeelDecorated(true);
        //setLocationRelativeTo(null);
        JScrollPane scroll = new JScrollPane(editor);

        //v.getContentPane()
        //v.getContentPane().add(scroll);
        v.getContentPane().add(scroll,BorderLayout.CENTER);
        // v.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
        // Marcamos el editor para que use HTML 
        editor.setContentType("text/html");

        // Insertamos un texto
        editor.setText("<h2>Comunicaci&oacute;n de datos</h2>\n" +
"<p><span id=\"t4_1\" class=\"t s3_1 f0\">Los datos e informaciones de control utilizados por la aplicaci&oacute;n son enviados o recibidos a&nbsp;</span><span id=\"t5_1\" class=\"t s3_1 f0\">trav&eacute;s de recursos de comunicaci&oacute;n de datos. Terminales y estaciones de trabajo son algunos&nbsp;</span><span id=\"t6_1\" class=\"t s3_1 f0\">ejemplos. Todos los dispositivos de comunicaci&oacute;n utilizan alg&uacute;n tipo de protocolo de&nbsp;</span><span id=\"t7_1\" class=\"t s3_1 f0\">comunicaci&oacute;n.</span></p>\n" +
"<p>Calificar el nivel de influencia en la aplicaci&oacute;n de acuerdo con la siguiente tabla:</p>\n" +
"<h2>&nbsp;</h2>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td><span id=\"tc_1\" class=\"t s3_1 f0\">Aplicaci&oacute;n puramente&nbsp;</span><span id=\"td_1\" class=\"t s6_1 f0\">batch&nbsp;</span><span id=\"te_1\" class=\"t s3_1 f0\">o funciona en una computadora aislada</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td><span id=\"tg_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n es&nbsp;</span><span id=\"th_1\" class=\"t s6_1 f0\">batch</span><span id=\"ti_1\" class=\"t s3_1 f0\">, pero utiliza entrada de datos remota o impresi&oacute;n&nbsp;</span><span id=\"tj_1\" class=\"t s3_1 f0\">remota</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td><span id=\"tl_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n es&nbsp;</span><span id=\"tm_1\" class=\"t s6_1 f0\">batch</span><span id=\"tn_1\" class=\"t s3_1 f0\">, pero utiliza entrada de datos remota e impresi&oacute;n&nbsp;</span><span id=\"to_1\" class=\"t s3_1 f0\">remota</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td><span id=\"tq_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n incluye entrada de datos&nbsp;</span><span id=\"tr_1\" class=\"t s6_1 f0\">on-line&nbsp;</span><span id=\"ts_1\" class=\"t s3_1 f0\">v&iacute;a entrada de v&iacute;deo o un&nbsp;</span><span id=\"tt_1\" class=\"t s3_1 f0\">procesador&nbsp;</span><span id=\"tu_1\" class=\"t s6_1 f0\">front-end&nbsp;</span><span id=\"tv_1\" class=\"t s3_1 f0\">para alimentar procesos&nbsp;</span><span id=\"tw_1\" class=\"t s6_1 f0\">batch&nbsp;</span><span id=\"tx_1\" class=\"t s3_1 f0\">o sistemas de&nbsp;</span><span id=\"ty_1\" class=\"t s3_1 f0\">consultas</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td><span id=\"t10_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n es m&aacute;s que una entrada&nbsp;</span><span id=\"t11_1\" class=\"t s6_1 f0\">on-line</span><span id=\"t12_1\" class=\"t s3_1 f0\">, y soporta apenas un&nbsp;</span><span id=\"t13_1\" class=\"t s3_1 f0\">protocolo de comunicaci&oacute;n</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td><span id=\"t15_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n es m&aacute;s que una entrada&nbsp;</span><span id=\"t16_1\" class=\"t s6_1 f0\">on-line&nbsp;</span><span id=\"t17_1\" class=\"t s3_1 f0\">y soporta m&aacute;s de un&nbsp;</span><span id=\"t18_1\" class=\"t s3_1 f0\">protocolo de comunicaci&oacute;n</span></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Procesamiento distribuido</h2>\n" +
"<p><span id=\"t1a_1\" class=\"t s3_1 f0\">Datos o procesamiento distribuidos entre varias unidades de procesamiento (</span><span id=\"t1b_1\" class=\"t s6_1 f0\">CPUs</span><span id=\"t1c_1\" class=\"t s3_1 f0\">) son&nbsp;</span><span id=\"t1d_1\" class=\"t s3_1 f0\">caracter&iacute;sticas generales que pueden influenciar en la complejidad de la aplicaci&oacute;n</span>:&nbsp;</p>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td><span id=\"t1h_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n no contribuye en la transferencia de datos o funciones entre&nbsp;</span><span id=\"t1i_1\" class=\"t s3_1 f0\">los procesadores de la empresa</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td><span id=\"t1k_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n prepara datos para el usuario final en otra&nbsp;</span><span id=\"t1l_1\" class=\"t s6_1 f0\">CPU&nbsp;</span><span id=\"t1m_1\" class=\"t s3_1 f0\">de la&nbsp;</span><span id=\"t1n_1\" class=\"t s3_1 f0\">empresa</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td><span id=\"t1p_1\" class=\"t s3_1 f0\">La aplicaci&oacute;n prepara datos para transferencia, los transfiere y entonces&nbsp;</span><span id=\"t1q_1\" class=\"t s3_1 f0\">son procesados en otro equipamiento de la empresa (no por el usuario&nbsp;</span><span id=\"t1r_1\" class=\"t s3_1 f0\">final)</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td><span id=\"t1t_1\" class=\"t s3_1 f0\">Procesamiento distribuido y la transferencia de datos son&nbsp;</span><span id=\"t1u_1\" class=\"t s6_1 f0\">on-line</span><span id=\"t1v_1\" class=\"t s3_1 f0\">, en&nbsp;</span><span id=\"t1w_1\" class=\"t s3_1 f0\">apenas una direcci&oacute;n</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td><span id=\"t1y_1\" class=\"t s3_1 f0\">Procesamiento distribuido y la transferencia de datos son&nbsp;</span><span id=\"t1z_1\" class=\"t s6_1 f0\">on-line</span><span id=\"t20_1\" class=\"t s3_1 f0\">, en&nbsp;</span><span id=\"t21_1\" class=\"t s3_1 f0\">ambas direcciones</span></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td><span id=\"t23_1\" class=\"t s3_1 f0\">Las funciones de procesamiento son din&aacute;micamente ejecutadas en el&nbsp;</span><span id=\"t24_1\" class=\"t s3_1 f0\">equipamiento m&aacute;s adecuado</span></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Objetivos de Rendimiento</h2>\n" +
"<p><span id=\"t2_2\" class=\"t s2_2 f0\">Los objetivos de rendimiento del sistema, establecidos y aprobados por el usuario en t&eacute;rminos&nbsp;</span><span id=\"t3_2\" class=\"t s2_2 f0\">de respuesta, influyen o podr&iacute;a influenciar el proyecto, desarrollo, implementaci&oacute;n o soporte de</span><span id=\"t4_2\" class=\"t s2_2 f0\">la aplicaci&oacute;n</span>:&nbsp;</p>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.23157654901974\">Ning&uacute;n requerimiento especial de perfomance fue solicitado por el usuario</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.179246607843\">Requerimientos de perfomance y de dise&ntilde;o fueron establecidos y previstos, sin embargo ninguna acci&oacute;n especial fue requerida</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.33499048039215\">El tiempo de respuesta y el volumen de datos son cr&iacute;ticos durante horarios pico de procesamiento. Ninguna determinaci&oacute;n especial para la utilizaci&oacute;n del procesador fue establecida. El intervalo de tiempo l&iacute;mite para la disponibilidad de procesamiento es siempre el pr&oacute;ximo d&iacute;a h&aacute;bil</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.3038417058823\">El tiempo de respuesta y volumen de procesamiento son items cr&iacute;ticos durante todo el horario comercial. Ninguna determinaci&oacute;n especial para la utilizaci&oacute;n del procesador fue establecida. El tiempo limite necesario para la comunicaci&oacute;n con otros sistemas es un aspecto importante</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.29138219607836\">Los requerimientos de perfomance establecidos requieren tareas de an&aacute;lisis de perfomance en la fase de an&aacute;lisis y dise&ntilde;o de la aplicaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.15308163725484\">Adem&aacute;s de lo descrito en el &iacute;tem anterior, herramientas de an&aacute;lisis</div>\n" +
"<div data-canvas-width=\"152.78100111764704\">de perfomance fueron usadas en las fases de dise&ntilde;o, desarrollo y/o</div>\n" +
"<div data-canvas-width=\"154.29109370588236\">implementaci&oacute;n para atender los requerimientos de perfomance establecidos por el usuario</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<p>&nbsp;</p>\n" +
"<h2>Configuraci&oacute;n Operacional Compartida</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">Esta caracter&iacute;stica representa la necesidad de realizar consideraciones especiales en el dise&ntilde;o de los sistemas para que la configuraci&oacute;n del equipamiento no sea sobrecargada:&nbsp;</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.22285489215693\">Ninguna restricci&oacute;n operacional expl&iacute;cita o impl&iacute;cita fue incluida</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.21039538235294\">Existen restricciones operacionales leves. No es necesario un esfuerzo especial para resolver estas restricciones</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.27518483333336\">Algunas consideraciones de ajuste de perfomance y seguridad son necesarias</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td>Son necesarias especificaciones especiales de procesador para un m&oacute;dulo espec&iacute;fico de la aplicaci&oacute;n</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.40102588235294\">Restricciones operacionales requieren cuidados especiales en el procesador central o procesador dedicado</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.0907840882353\">Adem&aacute;s de las caracter&iacute;sticas del &iacute;tem anterior, hay consideraciones especiales en la distribuci&oacute;n del sistema y sus componentes</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<p>&nbsp;</p>\n" +
"<h2>Tasa de Transacciones</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div data-canvas-width=\"294.8143843137256\">El nivel de transacciones es alto y tiene influencia en el dise&ntilde;o, desarrollo, implementaci&oacute;n y mantenimiento de la aplicaci&oacute;n:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.2415441568627\">No est&aacute;n previstos periodos picos de volumen de transacci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.3001038529411\">Est&aacute;n previstos picos de transacciones mensualmente, trimestral</div>\n" +
"<div data-canvas-width=\"152.65516006862745\">mente, anualmente o en un cierto periodo del a&ntilde;o</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td>Se prev&eacute;n picos semanales</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td>Se prev&eacute;n picos diariamente</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.21288728431375\">Alto nivel de transacciones fue establecido por el usuario, el tiempo de respuesta necesario exige un nivel alto o suficiente para requerir an&aacute;lisis de perfomance y dise&ntilde;o</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td>\n" +
"<div data-canvas-width=\"307.2016737254902\">Adem&aacute;s de lo descrito en el &iacute;tem anterior, es necesario utilizar herramientas de an&aacute;lisis de perfomance en las fases de dise&ntilde;o, desarrollo y/o implementaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Entrada de Datos en l&iacute;nea</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div data-canvas-width=\"170.13721764705883\">Esta caracter&iacute;stica cuantifica la entrada de datos on-line prove&iacute;da por la aplicaci&oacute;n:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td>Todas las transacciones son procesadas en modo batch</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td>De 1% al 7% de las transacciones son entradas de datos on-line</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td>De 8% al 15% de las transacciones son entradas de datos on-line</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td>De 16% al 23% de las transacciones son entradas de datos on-line</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td>De 24% al 30% de las transacciones son entradas de datos on-line</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td>M&aacute;s del 30% de las transacciones son entradas de datos on-line</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Interfaces con el Usuario</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div data-canvas-width=\"94.5923\">Las funciones on-line del sistema hacen &eacute;nfasis en la amigabilidad del sistema y su facilidad de uso, buscando aumentar la eficiencia del usuario final. El sistema posee:&nbsp;</div>\n" +
"<ul>\n" +
"<li data-canvas-width=\"94.5923\">\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"550.0537843137255\">Ayuda para la navegaci&oacute;n (teclas de funci&oacute;n, accesos directos y men&uacute;s din&aacute;micos)</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"550.0537843137255\">Men&uacute;s</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"41.0014862745098\">Documentaci&oacute;n y ayuda on-line</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"163.17332156862744\">Movimiento autom&aacute;tico del cursor</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"228.0955745098039\">Scrolling&nbsp;vertical y horizontal</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"50.533384313725485\">Impresi&oacute;n remota (a trav&eacute;s de transaccioneson-line)</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"301.74545294117655\">Teclas de funci&oacute;n preestablecidas</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"226.7615862745098\">Ejecuci&oacute;n de procesos batch a partir de transacciones on-line</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"365.65900784313726\">Selecci&oacute;n de datos v&iacute;a movimiento del cursor en la pantalla</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"217.90265098039217\">Utilizaci&oacute;n intensa de campos en video reverso, intensificados, subrayados, coloridos y otros indicadores</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"294.7338078431372\">Impresi&oacute;n de la documentaci&oacute;n de las transacciones on-line por medio de hard copy</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"352.97716666666673\">Utilizaci&oacute;n del mouse</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"101.0473705882353\">Men&uacute;s pop-up</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"45.503323529411766\">El menor n&uacute;mero de pantallas posibles para ejecutar las funciones del negocio</div>\n" +
"</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"94.5923\">\n" +
"<div data-canvas-width=\"263.1000823529413\">Soporte biling&uuml;e (el soporte de dos idiomas, cuente como cuatro items)</div>\n" +
"</li>\n" +
"<li data-canvas-width=\"94.5923\">\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"238.4809862745098\">Soporte multiling&uuml;e (el soporte de m&aacute;s de dos idiomas, cuente como seis items)</div>\n" +
"</li>\n" +
"</ul>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\" style=\"width: 476px;\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">Grado</td>\n" +
"<td style=\"width: 414px;\">Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">0</td>\n" +
"<td style=\"width: 414px;\">&nbsp;ning&uacute;n de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">1</td>\n" +
"<td style=\"width: 414px;\">&nbsp;De uno a tres de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">2</td>\n" +
"<td style=\"width: 414px;\">&nbsp;De cuatro a cinco de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">3</td>\n" +
"<td style=\"width: 414px; text-align: justify;\">M&aacute;s de cinco de los items descritos, no hay requerimientos espec&iacute;ficos del usuario en cuanto a amigabilidad del sistema</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">4</td>\n" +
"<td style=\"width: 414px;\">\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"307.38607447058826\">M&aacute;s de cinco de los items descritos, y fueron descritos requerimientos en cuanto a amigabilidad del sistema suficientes para generar actividades espec&iacute;ficas incluyendo factores tales como minimizaci&oacute;n de la digitaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">5</td>\n" +
"<td style=\"width: 414px; text-align: justify;\">M&aacute;s de cinco de los items descritos y fueron establecidos requerimientos en cuanto a la amigabilidad suficientes para utilizar herramientas especiales y procesos especiales para demostrar anticipadamente que los objetivos fueron alcanzados</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Actualizaciones en L&iacute;nea</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"267.7675490196079\">La aplicaci&oacute;n posibilita la actualizaci&oacute;n on-line de los archivos l&oacute;gicos internos:</div>\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"267.7675490196079\">&nbsp;</div>\n" +
"</div>\n" +
"<table class=\"editorDemoTable\" style=\"width: 472px;\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">Grado</td>\n" +
"<td style=\"width: 410px;\">Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">0</td>\n" +
"<td style=\"width: 410px; text-align: justify;\">&nbsp;Ninguna</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">1</td>\n" +
"<td style=\"width: 410px; text-align: justify;\">\n" +
"<div data-canvas-width=\"81.95990144117647\">Actualizaci&oacute;n on-line de uno a tres archivos l&oacute;gicos internos</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">2</td>\n" +
"<td style=\"width: 410px; text-align: justify;\">\n" +
"<div data-canvas-width=\"81.95990144117647\">Actualizaci&oacute;n on-line de m&aacute;s de tres archivos l&oacute;gicos</div>\n" +
"<div data-canvas-width=\"48.281846441176484\">internos</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">3</td>\n" +
"<td style=\"width: 410px; text-align: justify;\">Actualizaci&oacute;n on-line de la mayor&iacute;a de los archivos\n" +
"<div data-canvas-width=\"91.3419123235294\">l&oacute;gicos internos</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">4</td>\n" +
"<td style=\"width: 410px; text-align: justify;\">Adem&aacute;s del &iacute;tem anterior, la protecci&oacute;n contra p&eacute;rdidas de datos es esencial y fue espec&iacute;ficamente proyectado y codificado en el sistema</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">5</td>\n" +
"<td style=\"width: 410px; text-align: justify;\">\n" +
"<div data-canvas-width=\"307.1829844607842\">Adem&aacute;s del &iacute;tem anterior, altos vol&uacute;menes influyen en la las consideraciones de costo en el proceso de recuperaci&oacute;n. Procesos para automatizar la recuperaci&oacute;n fueron incluios minimizando la intervenci&oacute;n del operador</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Procesamiento Complejo</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div data-canvas-width=\"627.2684254901963\">\n" +
"<div data-canvas-width=\"310.9177411764706\">\n" +
"<div data-canvas-width=\"627.2684254901963\">El procesamiento complejo es una de las caracter&iacute;sticas de la&nbsp; aplicaci&oacute;n, los siguientes componentes est&aacute;n presentes:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"310.9177411764706\">&nbsp;</div>\n" +
"<div data-canvas-width=\"310.9177411764706\">\n" +
"<ul>\n" +
"<li data-canvas-width=\"267.7675490196079\">Procesamiento especial de auditoria y/o procesamiento especial de seguridad</li>\n" +
"<li data-canvas-width=\"267.7675490196079\">\n" +
"<div data-canvas-width=\"210.28966666666673\">Procesamiento l&oacute;gico extensivo</div>\n" +
"</li>\n" +
"<li data-canvas-width=\"267.7675490196079\">\n" +
"<div data-canvas-width=\"210.28966666666673\">Procesamiento matem&aacute;tico extensivo</div>\n" +
"</li>\n" +
"<li data-canvas-width=\"267.7675490196079\">\n" +
"<div data-canvas-width=\"250.8017254901961\">Gran cantidad de procesamiento de excepciones, resultando en transacciones incompletas que deber ser procesadas nuevame nte. Por ejemplo, transacciones de datos incompletas interrumpidas por problemas de comunicaci&oacute;n o con datos incompletos</div>\n" +
"</li>\n" +
"<li data-canvas-width=\"267.7675490196079\">\n" +
"<div data-canvas-width=\"215.10038039215684\">Procesamiento complejo para manipular m&uacute;ltiples posibilidades de entrada/salida. Ejemplo: multimedia</div>\n" +
"</li>\n" +
"</ul>\n" +
"</div>\n" +
"</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\" style=\"width: 483px;\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">Grado</td>\n" +
"<td style=\"width: 421px;\">Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">0</td>\n" +
"<td style=\"width: 421px;\">&nbsp;Ninguno de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">1</td>\n" +
"<td style=\"width: 421px;\">&nbsp;apenas uno de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">2</td>\n" +
"<td style=\"width: 421px;\">&nbsp;Dos de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">3</td>\n" +
"<td style=\"width: 421px;\">&nbsp;Tres de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">4</td>\n" +
"<td style=\"width: 421px;\">&nbsp;Cuatro de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">5</td>\n" +
"<td style=\"width: 421px;\">&nbsp;Todos los items descritos</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Reusabilidad del C&oacute;digo</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div data-canvas-width=\"310.9177411764706\">La aplicaci&oacute;n y su c&oacute;digo ser&aacute;n o fueron proyectados, desarrollados y mantenidos para ser utilizados en otras aplicaciones:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\" style=\"width: 475px;\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">Grado</td>\n" +
"<td style=\"width: 413px;\">Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">0</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">No presenta c&oacute;digo reutilizable&nbsp;</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">1</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">C&oacute;digo reutilizado fue usado solamente dentro de la\n" +
"<div data-canvas-width=\"58.76527799019609\">aplicaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">2</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">\n" +
"<div data-canvas-width=\"307.27144698039206\">Menos del 10% de la aplicaci&oacute;n fue proyectada previendo la utilizaci&oacute;n posterior del c&oacute;digo por otra aplicaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">3</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">10% o m&aacute;s de la aplicaci&oacute;n fue proyectada previendo la utilizaci&oacute;n posterior del c&oacute;digo por otra aplicaci&oacute;n</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">4</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">\n" +
"<div data-canvas-width=\"307.25649556862743\">La aplicaci&oacute;n fue espec&iacute;ficamente proyectada y/o documentada para tener su c&oacute;digo f&aacute;cilmente reutilizable por otra aplicaci&oacute;n y la aplicaci&oacute;n es configurada por el usuario a nivel de c&oacute;digo fuente</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">5</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">\n" +
"<div data-canvas-width=\"307.25649556862743\">La aplicaci&oacute;n fue espec&iacute;ficamente proyectada y/o documentada para tener su c&oacute;digo f&aacute;cilmente reutilizable por otra aplicaci&oacute;n y la aplicaci&oacute;n es configurada para uso a trav&eacute;s de par&aacute;metros que pueden ser alterados por el usuario</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p><strong>&nbsp;</strong></p>\n" +
"<h2>Facilidad de implementaci&oacute;n</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"327.09272156862744\">La facilidad de implementaci&oacute;n y conversi&oacute;n de datos son caracter&iacute;sticas de la aplicaci&oacute;n. Un plan de conversi&oacute;n e implementaci&oacute;n y/o herramientas de conversi&oacute;n fueron prove&iacute;das y</div>\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"352.35195294117653\">probadas durante la fase de prueba de la aplicaci&oacute;n:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td style=\"text-align: justify;\">\n" +
"<div data-canvas-width=\"307.2938740980391\">Ninguna consideraci&oacute;n especial fue establecida por el usuario y ning&uacute;n procedimiento especial fue</div>\n" +
"<div data-canvas-width=\"185.7687992745098\">necesario en la implementaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td style=\"text-align: justify;\">\n" +
"<div data-canvas-width=\"307.2938740980391\">Ninguna consideraci&oacute;n especial fue establecida por el usuario, m&aacute;s procedimientos especiales son requeridos en la implementaci&oacute;</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td style=\"text-align: justify;\">Requerimientos de conversi&oacute;n e implementaci&oacute;n fueron establecidos por el usuario y rutinas de de conversi&oacute;n e implementaci&oacute;n fueron proporcionados y probados. el impacto de conversi&oacute;n en el proyecto no es considerado\n" +
"<div data-canvas-width=\"65.02493571568628\">importante</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td style=\"text-align: justify;\">\n" +
"<div data-canvas-width=\"151.90385162745096\">Requerimientos de conversi&oacute;n e implementaci&oacute;n&nbsp;fueron establecidos por el usuario y rutinas de de&nbsp;conversi&oacute;n e implementaci&oacute;n fueron proporcionados y probados. el impacto de conversi&oacute;n en el proyecto es considerado\n" +
"<div data-canvas-width=\"65.02493571568628\">importante</div>\n" +
"</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td style=\"text-align: justify;\">\n" +
"<div data-canvas-width=\"307.2216089411764\">Adem&aacute;s del &iacute;tem 2, conversi&oacute;n autom&aacute;tica y&nbsp;herramientas de implementaci&oacute;n fueron proporcionadas y probadas</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td style=\"text-align: justify;\">\n" +
"<div data-canvas-width=\"307.2216089411764\">Adem&aacute;s del &iacute;tem 3, conversi&oacute;n autom&aacute;tica y herramientas de implementaci&oacute;n fueron prove&iacute;das</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p>&nbsp;</p>\n" +
"<h2>Facilidad de Operaci&oacute;n</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"531.5808823529413\">La facilidad de operaci&oacute;n es una caracter&iacute;stica del sistema. Procedimientos de inicializaci&oacute;n, respaldo y recuperaci&oacute;n fueron prove&iacute;dos y probados durante la fase de prueba del sistema. La aplicaci&oacute;n minimiza la necesidad de actividades manuales, tales como montaje de cintas magn&eacute;ticas, manoseo de papel e intervenci&oacute;n del operador:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td style=\"text-align: justify;\">Ninguna consideraci&oacute;n especial de operaci&oacute;n,adem&aacute;s del proceso normal de respaldo establecido por el usuario</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1-4</td>\n" +
"<td>Verificar cu&aacute;les de las siguientes afirmaciones pueden ser identificadas en la aplicaci&oacute;n. Cada &iacute;tem vale un punto, excepto se defina lo contrario:\n" +
"<ul>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"293.19593875490193\">Fueron desarrollados procedimientos de inicializaci&oacute;n y respaldo, siendo necesaria la intervenci&oacute;n del&nbsp; operador</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"293.19593875490193\">Se establecieron procesos de inicializaci&oacute;n,respaldo y recuperaci&oacute;n sin ninguna intervenci&oacute;n del operador (contar como 2 items)</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"293.19593875490193\">La aplicaci&oacute;n minimiza la necesidad de montaje de cintas magn&eacute;ticas</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"293.19593875490193\">La aplicaci&oacute;n minimiza la necesidad de manoseo de papel</li>\n" +
"</ul>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td>\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"307.22534679411757\">La aplicaci&oacute;n fue dise&ntilde;ada para trabajar sin operador, ninguna intervenci&oacute;n del operador es necesaria para operar el sistema, excepto ejecutar y cerrar la aplicaci&oacute;n. La aplicaci&oacute;n posee rutinas autom&aacute;ticas de recuperaci&oacute;n en caso de error</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p>&nbsp;</p>\n" +
"<h2>Instalaciones M&uacute;ltiples</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div style=\"text-align: justify;\" data-canvas-width=\"317.6488607843138\">La aplicaci&oacute;n fue espec&iacute;ficamente proyectada, dise&ntilde;ada e mantenida para ser instalada en m&uacute;ltiples locales de una organizaci&oacute;n o para m&uacute;ltiples organizaciones:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<table class=\"editorDemoTable\" style=\"width: 475px;\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">Grado</td>\n" +
"<td style=\"width: 413px;\">Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">0</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">\n" +
"<div data-canvas-width=\"150.7077386862745\">Los requerimientos del usuario no consideran la</div>\n" +
"<div data-canvas-width=\"254.92157058823528\">necesidad de instalaci&oacute;n de m&aacute;s de un local</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">1</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">La necesidad de m&uacute;ltiples locales fue considerada en el proyecto y la aplicaci&oacute;n fue dise&ntilde;ada para operar apenas sobre el mismo ambiente de hardware y software</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">2</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">La necesidad de m&uacute;ltiples locales fue considerada en el proyecto y la aplicaci&oacute;n fue dise&ntilde;ada para operar en ambientes similares de software y hardware</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">3</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">\n" +
"<div data-canvas-width=\"145.7226888137255\">La necesidad de m&uacute;ltiples locales fue considerada</div>\n" +
"<div data-canvas-width=\"146.84030684313726\">en el proyecto y la aplicaci&oacute;n est&aacute; separada para</div>\n" +
"<div data-canvas-width=\"291.50767517647057\">trabajar sobre diferentes ambientes de hardware</div>\n" +
"<div data-canvas-width=\"73.13981445098038\">y/o software</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">4</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">\n" +
"<div data-canvas-width=\"284.9925975000001\">Plan de mantenimiento y documentaci&oacute;n fueron proporcionados y probados para soportar la aplicaci&oacute;n en m&uacute;ltiples locales, adem&aacute;s los items 1 y 2 caracterizan a la aplicaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style=\"width: 44px;\">5</td>\n" +
"<td style=\"width: 413px; text-align: justify;\">\n" +
"<div data-canvas-width=\"285.4710426764706\">Plan de documentaci&oacute;n e mantenimiento fueron prove&iacute;dos y probados para soportar la aplicaci&oacute;n en m&uacute;ltiples locales, adem&aacute;s el &iacute;tem 3 caracteriza a la aplicaci&oacute;n</div>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<p>&nbsp;</p>\n" +
"<h2>Facilidad de Cambios</h2>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<div data-canvas-width=\"546.0040705882353\">La aplicaci&oacute;n fue espec&iacute;ficamente proyectada y dise&ntilde;ada con vistas a facilitar su mantenimiento. Las siguientes caracter&iacute;sticas pueden ser atribuidas a la aplicaci&oacute;n:&nbsp;</div>\n" +
"</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">&nbsp;</div>\n" +
"<div data-canvas-width=\"326.01389215686265\">\n" +
"<ul>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"530.2274960784315\">Est&aacute;n disponibles facilidades como consultas e informes flexibles para atender necesidades simples (contar 1 &iacute;tem)</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"530.2274960784315\">Est&aacute;n disponibles facilidades como consultas e informes flexibles para atender necesidades de complejidad media (contar 2 items)</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"530.2274960784315\">Est&aacute;n disponibles facilidades como consultas e informes flexibles para atendernecesidades complejas (contar 3 items)</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"530.2274960784315\">Datos de control son almacenados en tablas que son&nbsp; antenidas por el usuario a trav&eacute;s de procesos on-line, pero los cambios se hacen efectivos solamente al d&iacute;a siguiente</li>\n" +
"<li style=\"text-align: justify;\" data-canvas-width=\"530.2274960784315\">Datos de control son almacenados en tablas que son antenidas por el usuario a trav&eacute;s de procesos on-line, pero&nbsp; los cambios se hacen efectivos inmediatamente (contar 2 items)</li>\n" +
"</ul>\n" +
"<div data-canvas-width=\"476.25618235294115\">&nbsp;</div>\n" +
"<div data-canvas-width=\"476.25618235294115\">\n" +
"<table class=\"editorDemoTable\">\n" +
"<thead>\n" +
"<tr>\n" +
"<td>Grado</td>\n" +
"<td>Descripci&oacute;n</td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>0</td>\n" +
"<td>&nbsp;Ninguno de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>1</td>\n" +
"<td>&nbsp;apenas uno de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>2</td>\n" +
"<td>&nbsp;Dos de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>3</td>\n" +
"<td>&nbsp;Tres de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>4</td>\n" +
"<td>&nbsp;Cuatro de los items descritos</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>5</td>\n" +
"<td>&nbsp;Todos los items descritos</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</div>\n" +
"</div>");
        v.pack();

        v.setVisible(true);
}
}
