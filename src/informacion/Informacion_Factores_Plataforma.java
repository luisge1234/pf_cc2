/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package informacion;

import java.awt.Dimension;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 *
 * @author krlat
 */
public class Informacion_Factores_Plataforma extends JFrame{

    JFrame v = new JFrame("FACTORES PLATAFORMA");

    public void Informacion_Factores_Plataforma() {
        v.setPreferredSize(new Dimension(400, 550));
        JEditorPane editor = new JEditorPane();
        JFrame.setDefaultLookAndFeelDecorated(true);
        //setLocationRelativeTo(null);
        JScrollPane scroll = new JScrollPane(editor);

        //v.getContentPane()
        v.getContentPane().add(scroll);
        //v.getContentPane().add(scroll,BorderLayout.CENTER);
        // v.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
        // Marcamos el editor para que use HTML 
        editor.setContentType("text/html");

        // Insertamos un texto
        editor.setText("<h2 style=\"text-align: center;\">Factores de plataforma&nbsp;</h2>\n"
                + "<p style=\"text-align: justify;\" ><strong>TIME:&nbsp;</strong>Restricci&oacute;n del tiempo de ejecuci&oacute;n. Este factor representael grado de restricci&oacute;n de tiempo de ejecuci&oacute;n sobre el sistema de software.</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 110px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Normal</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td>1.11</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>1.29</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Extra Alto</td>\n"
                + "<td>1.63</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p style=\"text-align: justify;\"><strong>STOR:&nbsp;</strong>Restricci&oacute;n del almacenamiento principal. Este factor es una funci&oacute;n que representa el grado de restricci&oacute;n del almacenamiento principal impuesto sobre un sistema de software.&nbsp;</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 110px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td>1.05</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>1.17</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Extra Alto</td>\n"
                + "<td>1.46</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p style=\"text-align: justify;\"><strong>PVOL:&nbsp;</strong>Volatilidad de la plataforma. Este factor se usa para representar la frecuencia de los cambios enn la plataforma subyacente.&nbsp;</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 110px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td>0.87</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td>1.15</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>1.30</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p>&nbsp;</p>");

        // Se visualiza la ventana
        v.pack();

        v.setVisible(true);
    }
}
