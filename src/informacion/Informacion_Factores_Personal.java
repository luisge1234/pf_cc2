/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package informacion;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

/**
 *
 * @author krlat
 */
public class Informacion_Factores_Personal extends JFrame {

    JFrame v = new JFrame("FACTORES PERSONAL");
    //Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
    // int height = pantalla.height;
    //int width = pantalla.width;

    public void Informacion_Factores_Personal() {
        //setSize(width/2, height/2);

        v.setPreferredSize(new Dimension(400, 750));
        JEditorPane editor = new JEditorPane();
        JFrame.setDefaultLookAndFeelDecorated(true);
        //setLocationRelativeTo(null);
        JScrollPane scroll = new JScrollPane(editor);

        //v.getContentPane()
        v.getContentPane().add(scroll);
        //v.getContentPane().add(scroll,BorderLayout.CENTER);
        // v.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
        // Marcamos el editor para que use HTML 
        editor.setContentType("text/html");

        // Insertamos un texto
        editor.setText("<h2 style=\"text-align: center;\">Factores del personal&nbsp;</h2>\n"
                + "<p style=\"text-align: justify;\"><strong>ACAP:&nbsp;</strong>Capacidad de los analista, las personas que trabajan en el dise&ntilde;o global y dise&ntilde;o detallado de los requerimientos. Habilidad para el dise&ntilde;o, el an&aacute;lisis, la correcta comunicaci&oacute;n y cooperaci&oacute;n. No se tiene en cuenta el nivel de experiencia.&nbsp;&nbsp;</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 333px;\">\n"
                + "<tbody>\n"
                + "<tr style=\"height: 18px;\">\n"
                + "<td ><strong>Nivel</strong></td>\n"
                + "<td ><strong>Descripci&oacute;n</strong></td>\n"
                + "<td ><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy bajo</td>\n"
                + "<td>15 percentil.</td>\n"
                + "<td>1.42</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td>35 percentil.</td>\n"
                + "<td >1.19</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>55 percentil.</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td>75 percentil</td>\n"
                + "<td >0.85</td>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>90 percentil</td>\n"
                + "<td >0.71</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p>&nbsp;</p>\n"
                + "<p style=\"text-align: justify;\"><strong>AEXP:&nbsp;</strong>Experiencia en la aplicaci&oacute;n. Este factor mide el nivel de experiencia del equipo de desarrolo en aplicaciones similares.</p>\n"
                + "<table class=\"editorDemoTable\"style=\"width: 310px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td ><strong>Nivel</strong></td>\n"
                + "<td>&nbsp;<strong>Descripci&oacute;n&nbsp;</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Bajo</td>\n"
                + "<td>Experiencia menor o igual a 2 meses.</td>\n"
                + "<td>1.22</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td>Experiencia menor o igual a 6 meses.</td>\n"
                + "<td>1.10</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Normal</td>\n"
                + "<td>Experiencia de 1 a&ntilde;o.</td>\n" 
                + "<td>1</td>\n"
                + "</tr>\n"
                 + "<tr>\n"
                + "<td >Alto</td>\n"
                + "<td>Experiencia de 3 a&ntilde;o.</td>\n" 
                + "<td>0.88</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>Experiencia igual o mayor a 6 a&ntilde;os.</td>\n"
                + "<td>0.81</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p>&nbsp;</p>\n"
                + "<p style=\"text-align: justify;\"><strong>PCAP: </strong>Habilidad de los programadores. Se eval&uacute;a la capacidad de los programadores en&nbsp; el trabajo de equipo, tomando en cuenta aptitudes para comunicarse y cooperar mutuamnte.&nbsp;</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 333px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td>&nbsp;<strong>Descripci&oacute;n</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Bajo</td>\n"
                + "<td>15 percentil.</td>\n"
                + "<td>1.34</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td>35 percentil.</td>\n"
                + "<td>1.15</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>55 percentil.</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>&nbsp;Alto</td>\n"
                + "<td>75 percentil</td>\n"
                + "<td>0.88</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>&nbsp;Alto</td>\n"
                + "<td>90 percentil</td>\n"
                + "<td>0.76</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p style=\"text-align: justify;\"><strong>PEXP:&nbsp;</strong>Experiencia en la plataforma. Importancia del conocimientos de nuevas y potentes platformas, interfases, gr&aacute;ficas, base de datos, redes, etc.&nbsp;&nbsp;</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 333px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td>&nbsp;<strong>Descripci&oacute;n</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Muy Bajo</td>\n"
                + "<td>Menor o igual a 2 meses</td>\n"
                + "<td>1.19</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td text-align: justify;\">Menor o igual a 6 meses</td>\n"
                + "<td>1.09</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>1 a&ntilde;o</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<td>Alto</td>\n"
                + "<td>3 a&ntilde;o</td>\n"
                + "<td>0.91</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>Mayor o igual a 6 a&ntilde;os</td>\n"
                + "<td>0.85</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p style=\"text-align: justify;\"><strong>LTEX: </strong>Experiencia en el lenguaje de software y herramientes.&nbsp;</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 333px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td>&nbsp;<strong>Descripci&oacute;n</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Bajo</td>\n"
                + "<td>Menor o igual a 2 meses.</td>\n"
                + "<td>1.20</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td>Menor o igual a 6 meses.</td>\n"
                + "<td>1.09</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>1 a&ntilde;o.</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                 + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td>3 a&ntilde;o.</td>\n"
                + "<td>0.91</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>Mayor o igual a 6 a&ntilde;os.</td>\n"
                + "<td>0.84</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p>&nbsp;</p>\n"
                + "<p style=\"text-align: justify;\"><strong>PCON: </strong>Continuidad del personal.Mide el grado de permanencia anual del personal.&nbsp;&nbsp;</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 333px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td>&nbsp;<strong>Descripci&oacute;n</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Bajo</td>\n"
                + "<td>48% por a&ntilde;o.</td>\n"
                + "<td>1.29</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td>24% por a&ntilde;o.</td>\n"
                + "<td>1.12</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>12% por a&ntilde;o.</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td>6% por a&ntilde;o.</td>\n"
                + "<td>0.90</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>3% por a&ntilde;o.</td>\n"
                + "<td>0.81</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p>&nbsp;</p>");

        // Se visualiza la ventana
        setLocationRelativeTo(null);
        v.pack();

        v.setVisible(true);
    }
    /*public static void main(String[] args) {
        new Informacion_Factores_Personal();

    }*/

}
