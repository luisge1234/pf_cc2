/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package informacion;

import java.awt.Dimension;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 *
 * @author krlat
 */
public class Informacion_Factores_Proyecto extends JFrame{
      JFrame v = new JFrame("FACTORES DEL PROYECTO");
    
    public void Informacion_Factores_Proyecto() {
    v.setPreferredSize(new Dimension(470, 750));
        JEditorPane editor = new JEditorPane();
        JFrame.setDefaultLookAndFeelDecorated(true);
        //setLocationRelativeTo(null);
        JScrollPane scroll = new JScrollPane(editor);
        
        //v.getContentPane()
        v.getContentPane().add(scroll);
        //v.getContentPane().add(scroll,BorderLayout.CENTER);
        // v.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
           // Marcamos el editor para que use HTML 
        editor.setContentType("text/html");

        // Insertamos un texto
        editor.setText("<h2 style=\"text-align: center;\">Factores del proyecto&nbsp;</h2>\n"
                + "<p style=\"text-align: justify;\"><strong>TOOL:&nbsp;</strong> Uso de herramientas de software.&nbsp; \n El tipo de herramientas abarca desde las que permiten editar y codificar hasta las que posibilitan una administraci&oacute;n integral del desarrollo en todas sus etapas.</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 340px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td ><strong>Descripci&oacute;n</strong></td>\n"
                + "<td ><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr >\n"
                + "<td>Muy bajo</td>\n"
                + "<td >Herramientas que permiten editar, codificar, depurar.</td>\n"
                + "<td>1.17</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td >Herramientas simples con escasa integraci&oacute;n al proceso de desarrollo.</td>\n"
                + "<td>1.09</td>\n"
                + "</tr>\n"
                + "<tr >\n"
                + "<td >Normal</td>\n"
                + "<td >Herramientas b&aacute;sicas, integradas moderadamente</td>\n"
                + "<td >1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Alto</td>\n"
                + "<td >Herramientas robustas y maduras, integradas moderadamente.</td>\n"
                + "<td>0.90</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Muy Alto</td>\n"
                + "<td>Herramientas altamente integradas a los procesos, m&eacute;todos y reuso.</td>\n"
                + "<td >0.78</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p style=\"text-align: justify;\"><strong>SCED : </strong>Cronograma requerido para el desarrollo. Este factor mide la restricci&oacute;n en los plazos de tiempo impuesta al equipo de trabajo. Los valores se definen como un porcentaje de extensi&oacute;n o aceleraci&oacute;n de plazos con respecto al valor nominal. Acelerar los plazos produce m&aacute;s esfuerzo en las &uacute;ltimas etapas del desarrollo, en las que&nbsp;se acumulan m&aacute;s temas a determinar por la escacez de tiempo para resolverlos tempranamente. Por el contrario una relajaci&oacute;n de los plazos produce mayor esfuerzo en las etapas tempranas donde se destina m&aacute;s tiempo para las tareas de planificaci&oacute;n, especificaci&oacute;n, validaci&oacute;n cuidadosa y profunda.</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 340px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td><strong>Descripci&oacute;n</strong></td>\n"
                + "<td><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy bajo</td>\n"
                + "<td>75% del nominal.</td>\n"
                + "<td>1.43</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td>85% del nominal.</td>\n"
                + "<td>1.14</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Normal</td>\n"
                + "<td>100% del nominal.</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td>130% del nominal.</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Muy Alto</td>\n"
                + "<td>160% del nominal.</td>\n"
                + "<td>1</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p>&nbsp;</p>\n"
                + "<p style=\"text-align: justify;\"><strong>SITE:&nbsp;</strong>"+"Desarrollo multisitio. Coordinaci&oacute;n entre sitios que involucran disposci&oacute;n del equipo de trabajo y soporte de comunicaci&oacute;n. La determinaci&oacute;n de este factor de costo involucra la evaluaci&oacute;n y promedio de dos factores, ubicaci&oacute;n espacial (disposici&oacute;n del equipo de trabajo) y comunicaci&oacute;n (soporte de comunicaci&oacute;n).</p>\n"
                + "<table class=\"editorDemoTable\" style=\"width: 340px;\">\n"
                + "<tbody>\n"
                + "<tr>\n"
                + "<td><strong>Nivel</strong></td>\n"
                + "<td ><strong>Descripci&oacute;n</strong></td>\n"
                + "<td ><strong>Valor&nbsp;</strong></td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Muy bajo</td>\n"
                + "<td>Internacional.&nbsp;Alg&uacute;n tel&eacute;fono, mail.</td>\n"
                + "<td >1.22</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Bajo</td>\n"
                + "<td >Multi-ciudad y multi-compa&ntilde;&iacute;a.&nbsp;Tel&eacute;fonos individuales, FAX</td>\n"
                + "<td >1.09</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Normal</td>\n"
                + "<td>Multi-ciudad y multi-compa&ntilde;&iacute;a.&nbsp;Email de banda angosta</td>\n"
                + "<td >1</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td>Alto</td>\n"
                + "<td >Misma ciudad o &aacute;rea metropolitana.&nbsp;Comunicaciones electr&oacute;nicas de banda ancha.</td>\n"
                + "<td >0.93</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Muy Alto</td>\n"
                + "<td>Mismo Edificio o complejo.&nbsp;Comunicaciones electr&oacute;nicas de banda ancha, ocasionalmente videoconferencia.</td>\n"
                + "<td >0.86</td>\n"
                + "</tr>\n"
                + "<tr>\n"
                + "<td >Extra Alto</td>\n"
                + "<td >Completamente Centralizado.&nbsp;Multimedia Interactiva</td>\n"
                + "<td >0.80</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>\n"
                + "<p>&nbsp;</p>");

        // Se visualiza la ventana
        
        v.pack();

        v.setVisible(true);
}
}
