/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloPDF;

/**
 *
 * @author darkp
 */
public class ModelModulo {

    String nombre;
    double lineasCodigo;
    double esfuerzo;
    double tiempo;
    double cantidadPers;
    double sueldo;
    double imprevistos;
    double costoTotal;

    /**
     * Este metodo es un constructor de un modulo para poder generar el reporte
     * en formato pdf
     *
     * @param nombre representa el nombre de cada módulo ingresado
     * @param lineasCodigo las linas de codigo por puntos de funcion de cada
     * módulo
     * @param esfuerzo representa el esfuerzo en personas- mes para realizar el
     * módulo
     * @param tiempo represetna el tiempo de desarrollo del módulo
     * @param cantidadPers representa la cantidad de personas necesarias par
     * desarrollar el módulo
     * @param sueldo represente el valor a pagar a cada persana esta valor esta
     * dao dolares
     * @param imprevistos representa el total de imprevistos que se puede
     * generar al desarrollar el modulo que es el 10* del costo total
     * @param costoTotal representa el costo total del modulo tomando en cuenta
     * los imprevistos
     */
    public ModelModulo(String nombre, double lineasCodigo, double esfuerzo, double tiempo, double cantidadPers, double sueldo, double imprevistos, double costoTotal) {
        this.nombre = nombre;
        this.lineasCodigo = lineasCodigo;
        this.esfuerzo = esfuerzo;
        this.tiempo = tiempo;
        this.cantidadPers = cantidadPers;
        this.sueldo = sueldo;
        this.imprevistos = imprevistos;
        this.costoTotal = costoTotal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLineasCodigo() {
        return lineasCodigo;
    }

    public void setLineasCodigo(double lineasCodigo) {
        this.lineasCodigo = lineasCodigo;
    }

    public double getEsfuerzo() {
        return esfuerzo;
    }

    public void setEsfuerzo(double esfuerzo) {
        this.esfuerzo = esfuerzo;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }

    public double getCantidadPers() {
        return cantidadPers;
    }

    public void setCantidadPers(double cantidadPers) {
        this.cantidadPers = cantidadPers;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public double getImprevistos() {
        return imprevistos;
    }

    public void setImprevistos(double imprevistos) {
        this.imprevistos = imprevistos;
    }

    public double getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(double costoTotal) {
        this.costoTotal = costoTotal;
    }

}
