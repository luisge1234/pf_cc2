/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author krlat
 */

public class PF_No_ajustadosControlador {

    private static int ENTRADAS_EXTERNAS_BAJO = 3;
    private static int ENTRADAS_EXTERNAS_MEDIO = 4;
    private static int ENTRADAS_EXTERNAS_ALTO = 6;

    private static int SALIDAS_EXTERNAS_BAJO = 4;
    private static int SALIDAS_EXTERNAS_MEDIO = 5;
    private static int SALIDAS_EXTERNAS_ALTO = 7;

    private static int CONSULTAS_EXTERNAS_BAJO = 3;
    private static int CONSULTAS_EXTERNAS_MEDIO = 4;
    private static int CONSULTAS_EXTERNAS_ALTO = 6;

    private static int ARCHIVOS_LOGICOS_INTERNOS_BAJO = 7;
    private static int ARCHIVOS_LOGICOS_INTERNOS_MEDIO = 10;
    private static int ARCHIVOS_LOGICOS_INTERNOS_ALTO = 15;

    private static int ARCHIVOS_INTERFAZ_EXTERNOS_BAJO = 5;
    private static int ARCHIVOS_INTERFAZ_EXTERNOS_MEDIO = 7;
    private static int ARCHIVOS_INTERFAZ_EXTERNOS_ALTO = 10;

    /**
     * Este método nos permita calcular la cantidad de entradas externas
     * ingresadas por el usuario. Cada entrada se divide en tres categorias:
     * Bajo, Medio y Alto. Las entradas externas solo aceptan números mayores o
     * iguales a cero, además cada categoria tiene un valor constante por el que
     * es multiplicado: Bajo * 3, Medio * 4 y Alto * 6. Finalmente se realiza
     * la suma de los tres valores multiplicados.
     *
     * @param numEntradasExtBajo:variable númerica entera que nos permite
     * almace- nar el valor de las entradas externas de nivel Bajo que ingreso
     * el usuario
     * @param numEntradasExtMedio:variable númerica entera que nos permite
     * almace- nar el valor de las entradas externas de nivel Medio que ingreso
     * el usuario
     * @param numEntradasExtAlto:variable númerica entera que nos permite
     * almace- nar el valor de las entradas externas de nivel Alto que ingreso
     * el usuario
     * @return retorna la suma de todas las entradas externas, de los niveles:
     * Bajo, Medio y Alto.
     *
     *
     */
    public int calcularEntradasExternas(int numEntradasExtBajo, int numEntradasExtMedio, int numEntradasExtAlto) {
        int suma = 0;
        if (numEntradasExtBajo < 0 || numEntradasExtMedio < 0 || numEntradasExtAlto < 0) {
            throw new NumberFormatException("ingrese un número positivo");
        } else {
            suma = numEntradasExtBajo * ENTRADAS_EXTERNAS_BAJO + numEntradasExtMedio * ENTRADAS_EXTERNAS_MEDIO + numEntradasExtAlto * ENTRADAS_EXTERNAS_ALTO;

        }
        return suma;

    }

    /**
     * Este método nos permita calcular la cantidad de salidas externas
     * ingresadas por el usuario. Cada salida se divide en tres categorias:
     * Bajo, Medio y Alto. Las salidas externas solo aceptan números mayores o
     * iguales a cero, además cada categoria tiene un valor constante por el que
     * es multiplicado: Bajo * 4, Medio * 5 y Alto * 7.  Finalmente se realiza
     * la suma de los tres valores multiplicados.
     *
     * @param numSalidasExtBajo:variable númerica entera que nos permite almace-
     * nar el valor de las salidas externas de nivel Bajo que ingreso el usuario
     * @param numSalidasExtMedio:variable númerica entera que nos permite
     * almace- nar el valor de las salidas externas de nivel Medio que ingreso
     * el usuario
     * @param numSalidasExtAlto:variable númerica entera que nos permite almace-
     * nar el valor de las salidas externas de nivel Alto que ingreso el usuario
     * @return retorna la suma de todas las entradas externas, de los niveles:
     * Bajo, Medio y Alto.
     *
     *
     */
    public int calcularSalidasExternas(int numSalidasExtBajo, int numSalidasExtMedio, int numSalidasExtAlto) {
        int suma = 0;
        if (numSalidasExtBajo < 0 || numSalidasExtMedio < 0 || numSalidasExtAlto < 0) {
            throw new NumberFormatException("ingresar números positivos");
        } else {
            suma = numSalidasExtBajo * SALIDAS_EXTERNAS_BAJO + numSalidasExtMedio * SALIDAS_EXTERNAS_MEDIO + numSalidasExtAlto * SALIDAS_EXTERNAS_ALTO;
        }
        return suma;
    }

    /**
     * Este método nos permita calcular la cantidad de salidas externas
     * ingresadas por el usuario. Cada salida se divide en tres categorias:
     * Bajo, Medio y Alto. Las salidas externas solo aceptan números mayores o
     * iguales a cero, además cada categoria tiene un valor constante por el que
     * es multiplicado: Bajo * 3, Medio * 4 y Alto * 6. Finalmente se realiza
     * la suma de los tres valores multiplicados.
     *
     * @param numConsultasExtBajo:variable númerica entera que nos permite
     * almace- nar el valor de las salidas externas de nivel Bajo que ingreso el
     * usuario
     * @param numConsultasExtMedio:variable númerica entera que nos permite
     * almace- nar el valor de las salidas externas de nivel Medio que ingreso
     * el usuario
     * @param numConsultasExtAlto:variable númerica entera que nos permite
     * almace- nar el valor de las salidas externas de nivel Alto que ingreso el
     * usuario
     * @return retorna la suma de todas las consultas externas, de los niveles:
     * Bajo, Medio y Alto.
     *
     *
     */
    public int calcularConsultasExternas(int numConsultasExtBajo, int numConsultasExtMedio, int numConsultasExtAlto) {
        int suma = 0;
        if (numConsultasExtBajo < 0 || numConsultasExtMedio < 0 || numConsultasExtAlto < 0) {
            throw new NumberFormatException("ingresar números positivos");
        } else {
            suma = numConsultasExtBajo * CONSULTAS_EXTERNAS_BAJO + numConsultasExtMedio * CONSULTAS_EXTERNAS_MEDIO + numConsultasExtAlto * CONSULTAS_EXTERNAS_ALTO;
        }
        return suma;
    }

    /**
     * Este método nos permita calcular la cantidad de archivos lógicos internos
     * ingresadas por el usuario.Cada salida se divide en tres categorias:
 Bajo, Medio y Alto. Las salidas externas solo aceptan números mayores o
 iguales a cero, además cada categoria tiene un valor constante por el que
 es multiplicado: Bajo * 7, Medio * 10 y Alto * 15. Finalmente se realiza
 la suma de los tres valores multiplicados. 
     *
     * @param numArchivosLogicosIntBajo:variable númerica entera que nos permite
     * almacenar el valor de los archivos lógicos internos de nivel Bajo que 
     * ingreso el usuario.
     * @param numArchivosLogicosIntMedio:variable númerica entera que nos permite
     * almacenar el valor de los archivos lógicos internos de nivel Medio que ingreso
     * el usuario
     * @param numArchivosLogicosIntAlto 
     * @return retorna la suma de todos los archivos lógicos internos, de los niveles:
     * Bajo, Medio y Alto.
     *
     *
     */
    public int calcularArchivosLogicosInternos(int numArchivosLogicosIntBajo, int numArchivosLogicosIntMedio, int numArchivosLogicosIntAlto) {
        int suma = 0;
        if (numArchivosLogicosIntBajo < 0 || numArchivosLogicosIntMedio < 0 || numArchivosLogicosIntAlto < 0) {
            throw new NumberFormatException("Ingresar números positivos");
        } else {
            suma = numArchivosLogicosIntBajo * ARCHIVOS_LOGICOS_INTERNOS_BAJO + numArchivosLogicosIntMedio * ARCHIVOS_LOGICOS_INTERNOS_MEDIO + numArchivosLogicosIntAlto * ARCHIVOS_LOGICOS_INTERNOS_ALTO;
        }
        return suma;
    }
/**
     * Este método nos permita calcular la cantidad de archivos de interfaz 
     * externa ingresadas por el usuario. Cada salida se divide en tres categorias:
     * Bajo, Medio y Alto. Las salidas externas solo aceptan números mayores o
     * iguales a cero, además cada categoria tiene un valor constante por el que
     * es multiplicado: Bajo * 7, Medio * 10 y Alto * 15. Finalmente se realiza
     * la suma de los tres valores multiplicados. 
     *
     * @param numArchivosInterfazExtBajo:variable númerica entera que nos permite
     * almacenar el valor de archivos de interfaz externa de nivel Bajo que 
     * ingreso el usuario.
     * @param numArchivosInterfazExtMedio:variable númerica entera que nos permite
     * almacenar el valor de los archivos de interfaz externa de nivel Medio 
     * que ingreso el usuario.
     * @param numArchivosInterfazExttAlto:variable númerica entera que nos permite
     * almace- nar el valor de los archivos de interfaz externa de nivel Alto 
     * que ingreso el usuario.
     * @return retorna la suma de todos los archivos de interfaz externa, de los 
     * niveles: Bajo, Medio y Alto.
     *@return retorna la suma de todos los archivos de interfaz externa, de los 
     * niveles: Bajo, Medio y Alto.
     *
     */
    public int calcularArchivosInterfazExternos(int numArchivosInterfazExtBajo, int numArchivosInterfazExtMedio, int numArchivosInterfazExttAlto) {
        int suma = 0;
        if (numArchivosInterfazExtBajo < 0 || numArchivosInterfazExtMedio < 0 || numArchivosInterfazExttAlto < 0) {
            throw new NumberFormatException("Ingresar número positivos");
        } else {
            suma = numArchivosInterfazExtBajo * ARCHIVOS_INTERFAZ_EXTERNOS_BAJO + numArchivosInterfazExtMedio * ARCHIVOS_INTERFAZ_EXTERNOS_MEDIO + numArchivosInterfazExttAlto * ARCHIVOS_INTERFAZ_EXTERNOS_ALTO;
        }
        return suma;

    }
}
