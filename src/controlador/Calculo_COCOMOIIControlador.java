/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author krlat
 */
public class Calculo_COCOMOIIControlador {

    private double CONSTTIEMPODESARROLLO1 = 0.28;
    private double CONSTTIEMPODESARROLLO2 = 0.002;
    private double CONSTTIEMPODESARROLLO3 = 3.67;

    private double CONSTESFUERZO = 2.94;

    private double CONSTVALORIEMPO = 1.25;
    private double CONSTVALORIMPREVISTOS = 0.1;

    private double CONSTFACTORDEMULTI1 = 0.91;
    private double CONSTFACTORDEMULTI2 = 0.01;

    private double CONSTPFA1 = 0.65;
    private double CONSTPFA2 = 0.01;

    /**
     * Metodo para calcular el tiempo necesario para desarrollar el modulo
     *
     * @param numEsfuerzo valor del esfuerzo del modulo
     * @param numSumaFac valor de la suma de los factores del modulo
     * @return el tiempo necesaario para el desarrollo del modulo
     */
    public double calcular_TiempoDesarrollo(double numEsfuerzo, double numSumaFac) {
        double total = 0;
        if (numEsfuerzo < 0 || numSumaFac < 0) {
            throw new NumberFormatException("ingrese numeros positivos");
        } else {
            total = CONSTTIEMPODESARROLLO3 * Math.pow(numEsfuerzo, CONSTTIEMPODESARROLLO1 + CONSTTIEMPODESARROLLO2 * numSumaFac);

        }
        return total;
    }

    /**
     * Metodo con el cual calculamos el efuerzo requerido por modulo
     *
     * @param Ksloc lineas de codigo del modulo ya divididas para 1000
     * @param B factor exponencial de escala del modulo
     * @param Fac_Personal valor del factor de personal del modulo
     * @param Fac_Producto valor del factor de producto del modulo
     * @param Fac_Proyecto valor del factor de proyecto del modulo
     * @param Fac_Plataforma valor del factor de pltaforma del modulo
     * @return el valor total del esfuerzo requerido por modulo
     *
     */
    public double calcular_Esfuerzo(double Ksloc, double B, double Fac_Personal, double Fac_Producto, double Fac_Proyecto, double Fac_Plataforma) {
        double total = CONSTESFUERZO * Math.pow(Ksloc, B) * Fac_Personal * Fac_Producto * Fac_Proyecto * Fac_Plataforma;
        return total;
    }

    /**
     * Metodo que permite realizar el calculo del costo del modulo a desarrollar
     *
     * @param Sueldo valor del sueldo que se piensa pagar para hacer el modulo
     * @param TiempoDesarrollo tiempo necesario para desarrollar el modulo
     * @param CantPersonas cantidad de personas requeridas para desarrollar el
     * modulo
     * @return devuelve el costo total del modulo
     *
     */
    public double calcular_CostoModulo(double Sueldo, double TiempoDesarrollo, double CantPersonas) {
        double total = 0;
        if (Sueldo < 0) {
            throw new NumberFormatException("ingrese numeros positivos");
        } else {
            double tiempo = TiempoDesarrollo * CONSTVALORIEMPO;
            double modulo = Sueldo * tiempo * CantPersonas;
            double imprevistos = modulo * CONSTVALORIMPREVISTOS;
            total = modulo + imprevistos;
        }
        return total;
    }

    /**
     * Metodo para calcular el valor de los imprevistos por modulo
     *
     * @param Sueldo valor del sueldo que se piensa pagar para hacer el modulo
     * @param TiempoDesarrollo tiempo necesario para desarrollar el modulo
     * @param CantPersonas cantidad de personas requeridas para desarrollar el
     * modulo
     * @return devuelve el valor de los imprevistos necesarios por modulo
     *
     */
    public double calcular_Imprevistos(double Sueldo, double TiempoDesarrollo, double CantPersonas) {
        double imprevistos = 0;
        if (Sueldo < 0) {
            throw new NumberFormatException("ingrese numeros positivos");
        } else {
            double tiempo = TiempoDesarrollo * CONSTVALORIEMPO;
            double modulo = Sueldo * tiempo * CantPersonas;
            imprevistos = modulo * CONSTVALORIMPREVISTOS;
        }
        return imprevistos;
    }

    /**
     * Metodo para calcular los factores de escala 5 por modulo
     *
     * @param Prec Precedencia. Toma en cuenta el grado de experiencia previa en
     * relación al producto a desarrollar en aspectos organizacionales,
     * conocimiento del software y hadware a utilizar.
     * @param Flex Flexibilidad en el desarrollo. Considera el nivel de
     * exigencia en el cumpliento de los requerimientos preestablecidos, plazos
     * de tiempos y especificacioes de interfase.
     * @param Resl Arquitectura/Resolución de riesgo.
     * @param Team Cohesión de equipo. Tienen encuenta las dificultades de
     * sincronización entre los participantes del proyecto.
     * @param Pmat Madurez del proceso. 
     * @return el valor del factor de escala 5 por modulo
     *
     */
    public double calcularFactorEscala5(double Prec, double Flex, double Resl, double Team, double Pmat) {
        double total = Prec + Flex + Resl + Team + Pmat;
        return total;
    }

    /**
     * Metodo para calcular el factor de multiplicacion del modulo
     *
     * @param Suma valor total de la suma de todos los factores de escala 5 del
     * modulo
     * @return el valor total del factor de multiplicacion del modulo
     *
     */
    public double calcularFactorDeMultiplicacion(double Suma) {
        double total = 0;
        if (Suma < 0) {
            throw new NumberFormatException("Ingrese numeros positivos");
        } else {
            total = CONSTFACTORDEMULTI1 + CONSTFACTORDEMULTI2 * Suma;
        }
        return total;
    }

    /**
     * Metodo que permite calcular los puntos de funcion ajustados del modulo
     *
     * @param Pfna valor de los puntos de funcion no ajustados del modulo
     * @param Fc valor del factor de complejidad del modulo(valores obtenidos de
     * las 14 preguntas)
     * @return el valor total de los puntos de funcion ajustados del modulo
     *
     */
    public double calcularPFA(double Pfna, double Fc) {
        double total = 0;
        if (Pfna < 0 || Fc < 0) {
            throw new NumberFormatException("Ingrese numeros positivos");
        } else {
            total = Pfna * (CONSTPFA1 + (CONSTPFA2 * Fc));
        }
        return total;
    }

    /**
     * Metodo que permite hacer el calculo de la cantidad de personas necesarias
     * para desarrollar el modulo
     *
     * @param Esfuerzo valor del esfuerzo necesario del modulo
     * @param TiempoDesarrollo valor del tiempo requerido para desarrollar el
     * modulo
     * @return la cantidad de personas que se va a necesitar para poder
     * desarrollar el modulo
     */
    public double calcularCantidadPersonas(double Esfuerzo, double TiempoDesarrollo) {
        double cantidadPersonas = 0;
        if (Esfuerzo < 0 || TiempoDesarrollo < 0) {
            throw new NumberFormatException("Ingrese numeros positivos");
        } else {
            if (TiempoDesarrollo == 0) {
                cantidadPersonas = Esfuerzo / 1;
            } else {
                cantidadPersonas = Esfuerzo / TiempoDesarrollo;
                if (cantidadPersonas > 0 && cantidadPersonas < 1) {
                    cantidadPersonas = 1;
                } else {
                    return cantidadPersonas;
                }
            }
        }
        return cantidadPersonas;
    }

}
