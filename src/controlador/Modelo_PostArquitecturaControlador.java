/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author krlat
 */
public class Modelo_PostArquitecturaControlador {

    /**
     Este método nos permite realizar el cálculo del Factor Producto
     * @param numFactProdRELY : variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para medir la confiabilidad del producto.
     * Esta variable tiene 5 valores estáticos: Muy bajo: 0.82, Bajo: 0.92, 
     * Normal:1, Alto:1.10, Muy Alto: 1.26. Estos valores se los selcciona mediante un itemset.
     * @param numFactProdDATA: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para medir el nivel en que se encontrá la documentación. 
     * Esta variable tiene 4 valores estáticos: Bajo: 0.90, Normal:1, Alto:1.14, Muy Alto: 1.28  
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactProdDOCU: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para el tamaño de la BD. Esta variable 
     * tiene 5 valores estáticos: Muy bajo: 0.81, Bajo: 0.91,Normal:1, Alto:1.11, Muy Alto: 1.23. 
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactProdCPLX: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para medir la complejidad de las operaciones empleadas. 
     * Esta variable tiene 6 valores estáticos: Muy bajo: 0.73, Bajo: 0.87,Normal:1, 
     * Alto:1.17, Muy Alto: 1.34, Extra Alto: 1.74
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactProdRUSE: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario donde se considera el esfuerzo. 
     * Esta variable tiene 5 valores estáticos:  Bajo: 0.97,Normal:1, 
     * Alto:1.07, Muy Alto: 1.15, Extra Alto: 1.24
     * Estos valores se los selcciona mediante un itemset.
     *
     * @return  total: nos devuelve la multiplicación de todas la variables cada una con su valor seleccionado.
     **/
  public double calcular_FactoresProducto(double numFactProdRELY, double numFactProdDATA, double numFactProdDOCU, double numFactProdCPLX, double numFactProdRUSE){
      double total =  numFactProdRELY * numFactProdDATA* numFactProdDOCU * numFactProdCPLX * numFactProdRUSE;
      return total;
  }
      /**
     Este método nos permite realizar el cálculo del Factor Plataforma
     * @param numFacEstrTIME: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para representar el grado de restricción de tiempo de ejecución.
     * Esta variable tiene 4 valores estáticos: Normal:1, Alto:1.11, Muy Alto: 1.29, Extra Alto: 1.63 
     * Estos valores se los selcciona mediante un itemset.
     * @param numFacEstrSTOR: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para representar el grado de restricción de almacenamiento. 
     * Esta variable tiene 4 valores estáticos: Normal:1, Alto:1.05, Muy Alto: 1.17, Extra Alto: 1.46  
     * Estos valores se los selcciona mediante un itemset.
     * @param numFacEstrPVOL: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para representar la frecuencia de los cambios. 
     * tiene 4 valores estáticos: Bajo: 0.87, Normal:1, Alto:1.15, Muy Alto: 1.30   
     * Estos valores se los selcciona mediante un itemset.
     * @return  total: nos devuelve la multiplicación de todas la variables cada una con su valor seleccionado
     **/
    public double calcular_FactoresPlataforma(double numFacEstrTIME, double numFacEstrSTOR, double numFacEstrPVOL ) {
        double total = numFacEstrTIME * numFacEstrSTOR * numFacEstrPVOL;
        return total;
    }
    /**
     Este método nos permite realizar el cálculo del Factor Personal
     * @param numFactPersACAP: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para medir la capacidad de los analistas.
     * Esta variable tiene 5 valores estáticos: Muy bajo: 1.42, Bajo: 1.19, 
     * Normal:1, Alto:0.85, Muy Alto: 0.71.
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactPersAEXP: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para medir el nivel de experiencia del equipo. 
     * Esta variable tiene 5 valores estáticos: Muy bajo: 1.22, Bajo: 1.10, 
     * Normal:1, Alto:0.88, Muy Alto: 0.81.  
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactPersPCAP: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para evaluar la capacidad del trabajo en equipo. 
     * Esta variable tiene 5 valores estáticos: 
     * Muy bajo: 1.34 Bajo: 1.15, Normal:1, Alto:0.88, Muy Alto: 0.81 
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactPersPEXP: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para la experiencia d ela plataforma. 
     * Esta variable tiene 5 valores estáticos: 
     * Muy bajo: 1.19 Bajo: 1.09, Normal:1, Alto:0.91, Muy Alto: 0.85 
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactPersLTEX: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario donde se considera la experiencia en el lenguaje de SF y herramientas. 
     * Esta variable tiene 5 valores estáticos: Muy Bajo: 1.20, Bajo: 1.09,Normal:1, 
     * Alto:0.91, Muy Alto: 0.84
     * Estos valores se los selcciona mediante un itemset.
     * @param numFactPersPCON: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario mide el grado de permanencia anual de personal. 
     * Esta variable tiene 5 valores estáticos: Muy Bajo: 1.29, Bajo: 1.12,Normal:1, 
     * Alto:0.90, Muy Alto: 0.81
     * Estos valores se los selcciona mediante un itemset.
     *
     * @return  total: nos devuelve la multiplicación de todas la variables cada una con su valor seleccionado.
     * **/
     public double calcular_FactoresPersonal(double numFactPersACAP, double numFactPersAEXP, double numFactPersPCAP, double numFactPersPEXP, double numFactPersLTEX,double numFactPersPCON){
         double total = numFactPersACAP * numFactPersAEXP * numFactPersPCAP * numFactPersPEXP * numFactPersLTEX * numFactPersPCON;
         return total;
     }
       /**
     Este método nos permite realizar el cálculo del Factor Proyecto
     * @param numFacProySTOOL: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para representar el gtipo deherramientas de software.
     * Esta variable tiene 5 valores estáticos: Muy Bajo: 1.17 Bajo: 1.09 Normal:1, Alto:0.90, Muy Alto: 0.78
     * Estos valores se los selcciona mediante un itemset.
     * @param numFacProySCED: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para representar el cronograma requerido para el desarrollo. 
     * Esta variable tiene 5 valores estáticos: Muy Bajo: 1.43 Bajo: 1.14 Normal:1, Alto:1, Muy Alto: 1
     * Estos valores se los selcciona mediante un itemset.
     * @param numFacProySITE: variable númerica decimal que nos permite almacenar
     * el valor seleccionado por el usuario para representar el desarrollo multisitio. 
     * Esta variable tiene 5 valores estáticos: Muy Bajo: 1.22 Bajo: 1.09 Normal:1, Alto:0.93, Muy Alto: 0.86, Extra Alto: 0.80
  
     * Estos valores se los selcciona mediante un itemset.
     * @return  total: nos devuelve la multiplicación de todas la variables cada una con su valor seleccionado
     **/
    public double calcular_FactoresProyecto(double numFacProySTOOL, double numFacProySCED, double numFacProySITE ){
        double total = numFacProySTOOL * numFacProySCED * numFacProySITE;
        return total;
    }
    
   

}
