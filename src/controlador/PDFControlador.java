/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import ModeloPDF.ModelModulo;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.ListItem;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author darkp
 *
 */
public class PDFControlador {

    // public static final String logo = "C:\\Users\\darkp\\OneDrive\\Documents\\pf_cc2\\src\\resources\\images\\logoUNL.png";
    public String logo = getClass().getResource("/resources/images/logoUNL.png").toString();

    /**
     * Este metodo permite la creacion del archivo pdf con los datos que se
     * presentan en la pantalla principal
     *
     * @param destino ruta para guardar el archivo
     * @param listaModulo lista de los modulos generados en la vista principal
     * @param lineasCodTotal valor total de las lineas de codigo del proyecto
     * @param esfuerzoTotal valor total del esfuerzo del proyecto
     * @param tiempoTotal valor total del tiempo del proyecto
     * @param cantiPersoTotal cantidad total las personas necesarias para el
     * proyecto
     * @param sueldoTotal valor total de los sueldos para el proyecto
     * @param imprevistosTotal valor total necesario para imprevistos del
     * proyecto
     * @param costoTotalProyecto costo total del proyecto
     * @throws java.io.IOException excepcion producida por operaciones de E/S
     * fallidas o interrumpidas.
     *
     *
     */
    public void CreatePDF(
            String destino,
            List<ModelModulo> listaModulo,
            double lineasCodTotal,
            double esfuerzoTotal,
            double tiempoTotal,
            double cantiPersoTotal,
            double sueldoTotal,
            double imprevistosTotal,
            double costoTotalProyecto
    ) throws IOException {
        PdfWriter pdfWriter = new PdfWriter(destino);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);

        //Creo el Docuemento con sus propiedades 
        Document document = new Document(pdfDocument, PageSize.A4.rotate());

        //Fuentes
        PdfFont pdfFontBold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        PdfFont pdfFontSimple = PdfFontFactory.createFont(FontConstants.HELVETICA);

        //Creo el diseño de mi documento PDF
        Image logoUNL = new Image(ImageDataFactory.create(logo)).scaleAbsolute(300, 100);
        document.add(logoUNL);
        Date fecha = new Date();
        SimpleDateFormat objSDF = new SimpleDateFormat("dd-MM-yyyy");
        document.add(new Paragraph(""));
        document.add(new Paragraph("Loja, " + objSDF.format(fecha)));
        document.add(new Paragraph("Integrantes: "));
        com.itextpdf.layout.element.List list = new com.itextpdf.layout.element.List().setSymbolIndent(12).setListSymbol("\u2022").setFont(pdfFontSimple);
        list.add(new ListItem("Jean Alvarado"));
        list.add(new ListItem("Luis Mocha"));
        list.add(new ListItem("Carla Troya"));
        document.add(list);

        document.add(new Paragraph("Docente: "));
        com.itextpdf.layout.element.List list2 = new com.itextpdf.layout.element.List().setSymbolIndent(12).setListSymbol("\u2022").setFont(pdfFontSimple);
        list2.add(new ListItem("Ing. Mgs. José Guamán"));
        document.add(list2);

        document.add(new Paragraph(""));
        document.add(new Paragraph("Datos obtenidos por Modulos: "));
        document.add(new Paragraph(""));
        //Agrego la tabla al documento 
        Table table = new Table(8);
        table.setHorizontalAlignment(HorizontalAlignment.CENTER);
        table.setTextAlignment(TextAlignment.CENTER);

        //Aqui se agrega las cabeceras de la tabla
        table.addHeaderCell(new Cell().add(new Paragraph("Nombre")).setFont(pdfFontBold));
        table.addHeaderCell(new Cell().add(new Paragraph("Lineas de Codigo")).setFont(pdfFontBold));
        table.addHeaderCell(new Cell().add(new Paragraph("Esfuerzo")).setFont(pdfFontBold));
        table.addHeaderCell(new Cell().add(new Paragraph("Tiempo")).setFont(pdfFontBold));
        table.addHeaderCell(new Cell().add(new Paragraph("Cantidad de Personas")).setFont(pdfFontBold));
        table.addHeaderCell(new Cell().add(new Paragraph("Sueldo")).setFont(pdfFontBold));
        table.addHeaderCell(new Cell().add(new Paragraph("Imprevistos")).setFont(pdfFontBold));
        table.addHeaderCell(new Cell().add(new Paragraph("Costo Total")).setFont(pdfFontBold));

        //En base a la lista de modulos genero la presentacion        
        for (ModelModulo modelModulo : listaModulo) {
            table.addCell(new Paragraph(
                    modelModulo.getNombre()
            )).setFont(pdfFontSimple);
            table.addCell(new Paragraph(
                    String.valueOf(modelModulo.getLineasCodigo())
            )).setFont(pdfFontSimple);
            table.addCell(new Paragraph(
                    String.valueOf(modelModulo.getEsfuerzo())
            )).setFont(pdfFontSimple);
            table.addCell(new Paragraph(
                    String.valueOf(modelModulo.getTiempo())
            )).setFont(pdfFontSimple);
            table.addCell(new Paragraph(
                    String.valueOf(modelModulo.getCantidadPers())
            )).setFont(pdfFontSimple);
            table.addCell(new Paragraph(
                    String.valueOf(modelModulo.getSueldo())
            )).setFont(pdfFontSimple);
            table.addCell(new Paragraph(
                    String.valueOf(modelModulo.getImprevistos())
            )).setFont(pdfFontSimple);
            table.addCell(new Paragraph(
                    String.valueOf(modelModulo.getCostoTotal())
            )).setFont(pdfFontSimple);
        }
        document.add(table);

        document.add(new Paragraph(""));
        document.add(new Paragraph("Datos obtenidos por Proyecto: "));
        document.add(new Paragraph(""));

        Table tableProyecto = new Table(7);
        tableProyecto.setHorizontalAlignment(HorizontalAlignment.CENTER);
        tableProyecto.setTextAlignment(TextAlignment.CENTER);

        tableProyecto.addHeaderCell(new Cell().add(new Paragraph("Lineas de Codigo")).setFont(pdfFontBold));
        tableProyecto.addHeaderCell(new Cell().add(new Paragraph("Esfuerzo (Persona-Mes)")).setFont(pdfFontBold));
        tableProyecto.addHeaderCell(new Cell().add(new Paragraph("Tiempo D. (Meses)")).setFont(pdfFontBold));
        tableProyecto.addHeaderCell(new Cell().add(new Paragraph("Cantidad Personas")).setFont(pdfFontBold));
        tableProyecto.addHeaderCell(new Cell().add(new Paragraph("$ Sueldo")).setFont(pdfFontBold));
        tableProyecto.addHeaderCell(new Cell().add(new Paragraph("Imprevistos (10%)")).setFont(pdfFontBold));
        tableProyecto.addHeaderCell(new Cell().add(new Paragraph("$ Costo Total")).setFont(pdfFontBold));

        tableProyecto.addCell(new Paragraph(String.valueOf(lineasCodTotal))).setFont(pdfFontSimple);
        tableProyecto.addCell(new Paragraph(String.valueOf(esfuerzoTotal))).setFont(pdfFontSimple);
        tableProyecto.addCell(new Paragraph(String.valueOf(tiempoTotal))).setFont(pdfFontSimple);
        tableProyecto.addCell(new Paragraph(String.valueOf(cantiPersoTotal))).setFont(pdfFontSimple);
        tableProyecto.addCell(new Paragraph(String.valueOf(sueldoTotal))).setFont(pdfFontSimple);
        tableProyecto.addCell(new Paragraph(String.valueOf(imprevistosTotal))).setFont(pdfFontSimple);
        tableProyecto.addCell(new Paragraph(String.valueOf(costoTotalProyecto))).setFont(pdfFontSimple);

        document.add(tableProyecto);

        document.close();
        pdfDocument.close();
        pdfWriter.close();
    }
}
